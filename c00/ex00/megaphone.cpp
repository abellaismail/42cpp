#include <cctype>
#include <iostream>
#include <iterator>

void ft_upper(char *str)
{
	for(int i = 0; str[i]; i++)
		str[i] = std::toupper(str[i]);
}

int main (int ac, char *av[])
{
	if (ac > 1)
	{
		int i = 1;
		while (i < ac)
		{
			ft_upper(av[i]);
			std::cout << av[i++];
		}
	}
	else
	{
		std::cout << "* LOUD AND UNBEARABLE FEEDBACK NOISE *";
	}
	std::cout << std::endl;
	return 0;
}
