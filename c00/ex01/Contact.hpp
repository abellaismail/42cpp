
#ifndef CONTACT_HPP
#define CONTACT_HPP
#include <string>
#include <iostream>


class Contact
{
	public:
		void setfname(std::string &fname);
		void setlname(std::string &lname);
		void set_nickname(std::string &nickname);
		void set_phonenumber(std::string &phone_number);
		void set_secret(std::string &secret);
		
		std::string &getfname();
		std::string &getlname();
		std::string &get_nickname();
		std::string &get_phonenumber();
		std::string &get_secret();
	private:
		std::string fname;
		std::string lname;
		std::string nickname;
		std::string phone_num;
		std::string secret;
		
};

std::istream &operator>>(std::istream &is, Contact &contact);
#endif
