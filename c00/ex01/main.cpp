#include "PhoneBook.hpp"
#include <ios>
#include <ostream>


int main ()
{
	PhoneBook pb;
	std::string line;
	
	while(1)
	{
		std::cout << "> " << std::flush;
		std::getline(std::cin, line);
		if (line == "EXIT" || std::cin.eof())
			break;
		else if (line == "ADD") 
			pb.add();
		else if (line == "SEARCH")
			pb.search();
	}

	return 0;
}
