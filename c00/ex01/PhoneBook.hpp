#ifndef PHONEBOOK_HPP
#define PHONEBOOK_HPP

#include "Contact.hpp"
#include <cstdio>
#include <iostream>
#include <ostream>
#include <string>

void print_cell(std::string str, int isLast);
class PhoneBook {

public:
  PhoneBook();
  void add();
  void search();

private:
  int counter;
  int is_filled;
  Contact contacts[8];
  void display(int index);
};
#endif
