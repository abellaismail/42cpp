#include "Contact.hpp"
#include <cstdlib>
#include <string>

std::string fill(std::istream &is, const std::string &str)
{
	std::string line;
	while (line.empty())
	{
		std::cout << str << std::flush;
		std::getline(is, line);
		if (std::cin.eof())
			exit(1);
	}
	return line;
}

std::istream &operator>>(std::istream &is, Contact &contact)
{
	std::string ret;

	ret = fill(is, "First Name: ");
	contact.setfname(ret);

	ret = fill(is, "Last Name: ");
	contact.setlname(ret);

	ret = fill(is, "Nickname: ");
	contact.set_nickname(ret);

	ret = fill(is, "Phone number: ");
	contact.set_phonenumber(ret);

	ret = fill(is, "Darkest Secret: ");
	contact.set_secret(ret);
	return is;
}

void Contact::setfname(std::string &fname)
{ this->fname = fname; }

void Contact::setlname(std::string &lname)
{ this->lname = lname; }

void Contact::set_nickname(std::string &nickname)
{ this->nickname = nickname; }

void Contact::set_phonenumber(std::string &phone_number)
{ this->phone_num = phone_number; }

void Contact::set_secret(std::string &secret)
{ this->secret = secret; }

std::string &Contact::getfname()
{ return this->fname; }

std::string &Contact::getlname()
{ return this->lname; }

std::string &Contact::get_nickname()
{ return this->nickname; }

std::string &Contact::get_phonenumber()
{ return this->phone_num; }

std::string &Contact::get_secret()
{ return this->secret; }
