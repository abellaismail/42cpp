#include "PhoneBook.hpp"
#include <cstdio>
#include <iostream>
#include <iomanip>
#include <ostream>
#include <string>

void print_cell(std::string str, int isLast) {
  std::cout << '|';
  if (str.length() > 10)
  	std::cout << str.substr(0, 9) << '.';
  else
  	std::cout <<  std::setw(10) <<  str;
  if (isLast)
    std::cout << "|\n";
}

PhoneBook::PhoneBook() {
  counter = 0;
  is_filled = 0;
};

void PhoneBook::add() {
  Contact *contact = contacts + counter;
  std::cin >> *contact;
  counter++;
  if (counter == 8) {
    counter = 0;
    is_filled = 1;
  }
}

void PhoneBook::search() {
  print_cell("index", 0);
  print_cell("first name", 0);
  print_cell("last name", 0);
  print_cell("nickname", 1);
  int size = is_filled ? 8 : counter;
  int i = 0;
  while (i < size) {
  	std::cout << '|' << std::setw(10) << i;
    print_cell(contacts[i].getfname(), 0);
    print_cell(contacts[i].getlname(), 0);
    print_cell(contacts[i].get_nickname(), 1);
    i++;
  }
  if (!is_filled && counter == 0)
    return;
  std::cout << "Please Pick a number: " << std::flush;
  std::string line;
  std::getline(std::cin, line);
  if (line.size() == 1)
    display(line[0] - '0');
}

void PhoneBook::display(int index) {
  if ((!is_filled && index >= counter ) || (index < 0 && index > 7))
    return;

  Contact *contact = contacts + index;

  std::cout << "First Name: " << contact->getfname() << '\n'
            << "Last Name: " << contact->getlname() << '\n'
            << "Nickname: " << contact->get_nickname() << '\n'
            << "Phone Number: " << contact->get_phonenumber() << '\n'
            << "Darkest Secret: " << contact->get_secret() << std::endl;
}
