#include "Span.hpp"
#include <cstdlib>
#include <vector>


void letsgorandom(Span &sp, int n)
{
	srand(time(NULL));
	while (n--)
	{
		int r = rand();
		//std::cout << r << ' ';
		sp.addNumber(r);
	}
	std::cout << std::endl;

}

int main ()
{
	Span sp(1000000);
	std::vector<int> n;
	// n.push_back(1);
	// n.push_back(1);
	// n.push_back(7);
	// n.push_back(9);
	// n.push_back(-2);

	sp.addRangOfNumbers(n.begin(), n.end());
	letsgorandom(sp, 1000000 - 1);
	std::cout << sp.shortestSpan() << std::endl;
	std::cout << sp.longestSpan() << std::endl;
	return 0;
}
