#ifndef SPAN_HPP
# define SPAN_HPP

#include <iostream>
#include <set>
#include <vector>

#define VEC_ITER std::vector<int>::iterator

class Span
{
public:
	Span();
	Span(unsigned int n);
	Span(const Span &);
	Span&operator=(const Span&);
	~Span();

	void addNumber(int number);
	int shortestSpan();	
	int longestSpan();
	void addRangOfNumbers(VEC_ITER iter, VEC_ITER end);
 
protected:
private:
	std::multiset<int> vec;
	unsigned int size;
};
#endif
