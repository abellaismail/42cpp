#include "Span.hpp"
#include <iterator>
#include <limits.h>
#include <algorithm>

Span::Span(): size(0) { }

Span::Span(unsigned int n): size(n){ }

Span::Span(const Span &span): vec(span.vec), size(span.size)
{ }

Span &Span::operator=(const Span &span)
{
	vec = span.vec;
	size = span.size;
	return *this;
}

Span::~Span() { }

void Span::addNumber(int num) {
	if (vec.size() >= size)
	{
		std::cout << "Span is full" << std::endl;
		return;
	}
	vec.insert(num);
}


int	Span::longestSpan() {
	if (vec.size() <= 1)
		throw std::invalid_argument("Not enough elements");
	return *(vec.rbegin()) - *(vec.begin());
}

int	Span::shortestSpan() {
	if (vec.size() <= 1)
		throw std::invalid_argument("Not enough elements");
	std::multiset<int>::iterator it = vec.begin();
	long long shortestSpan = INT_MAX;
	int last = *it;
	it++;
	for(size_t i = 1; i < vec.size() ; it++, i++)
	{
		if (shortestSpan > (*it - last))
			shortestSpan = *it - last;
		last = *it;
	}
	return shortestSpan;
}

void Span::addRangOfNumbers(VEC_ITER begin, VEC_ITER end)
{
	for(; begin != end ; begin++)
		addNumber(*begin);
}
