#include "MutantStack.hpp"
#include <stack>
using std::stack;

int main ()
{
	stack<int> st;
	st.push(13);
	st.push(37);
	st.push(42);
	st.push(0);
	stack<int>::iterator it = st.begin();
	stack<int>::iterator ite = st.end();
	while (it != ite)
	{
		std::cout << *it << std::endl;
		it++;
	}
	return 0;
}
