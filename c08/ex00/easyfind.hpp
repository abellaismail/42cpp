#ifndef EASYFIND_HPP
# define EASYFIND_HPP

template<typename T>
bool easyfind(T &v, int i)
{
	typename T::iterator iter;
	iter = find(v.begin(), v.end(), i);
	return iter != v.end();
}

#endif
