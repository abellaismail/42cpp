#include <iostream>
#include <vector>
#include "easyfind.hpp"


int main ()
{
	
	std::vector<int> vect;
	vect.push_back(13);
	vect.push_back(37);
	vect.push_back(42);
	vect.push_back(21);
	vect.push_back(0);

	if (easyfind(vect, 32))
		std::cout << "found" << std::endl;
	else
		std::cout << "really?" << std::endl;
	return 0;
}
