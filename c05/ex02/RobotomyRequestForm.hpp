#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP
#include "Form.hpp"
#include <iostream>
#include <string>
class RobotomyRequestForm: public Form
{
public:
	RobotomyRequestForm();
	RobotomyRequestForm(const std::string &target);
	RobotomyRequestForm(const RobotomyRequestForm &);
	RobotomyRequestForm&operator=(const RobotomyRequestForm&);
	~RobotomyRequestForm();
 
	void execute() const;
private:
	std::string target;

};
#endif
