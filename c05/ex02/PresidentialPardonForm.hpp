#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <iostream>
#include <string>
class PresidentialPardonForm: public Form
{
public:
	PresidentialPardonForm();
	PresidentialPardonForm(const std::string &target);
	PresidentialPardonForm(const PresidentialPardonForm &);
	PresidentialPardonForm&operator=(const PresidentialPardonForm&);
	~PresidentialPardonForm();

	void execute() const;
 
protected:
private:
	std::string target;

};
#endif
