#ifndef FORM_HPP
# define FORM_HPP
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"

class Form
{
public:
	Form();
	Form(std::string name, int sign, int exec);
	Form(Form&);
	Form(const Form &);
	Form&operator=(const Form&);
	void execute_(Bureaucrat const &b) const;
	virtual void execute() const = 0;
	virtual ~Form() = 0;

	class GradeTooHighException: public std::invalid_argument {
	    public:
			GradeTooHighException();
	};

	class GradeTooLowException : public std::invalid_argument {
	    public:
			GradeTooLowException();
	};

	bool getSign() const;
	const std::string &getName() const;
	int getSignGrade() const;
	int getExecGrade() const;

	void beSigned(const Bureaucrat &b);
 
protected:
private:
	const std::string name;
	const int sign_grade;
	const int exec_grade;
	bool _signed;
};

std::ostream &operator<<(std::ostream & os, const Form&);
#endif
