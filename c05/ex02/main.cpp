#include "Bureaucrat.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "iostream"
#include "Form.hpp"
#include <exception>

int main() {

	Bureaucrat	b("b0", 120);
	ShrubberyCreationForm	shrubbery("Shrubbery");

	Bureaucrat b1("b1", 130);
	RobotomyRequestForm robot("RobotomyRequest");

	Bureaucrat b2("b2", 1);
	PresidentialPardonForm pardon("Presidential");

	std::cout << "------------" << std::endl;
	try {
		std::cout << shrubbery << std::endl;
		b.signForm(shrubbery);
		b.executeForm(shrubbery);
		std::cout << "------------" << std::endl;

		//b1.signForm(robot);
		//b1.executeForm(robot);
		//std::cout << "------------" << std::endl;

		b2.signForm(pardon);
		b2.executeForm(pardon);
	} catch (std::exception &e) {
		std::cout << e.what() << std::endl;
	}
	std::cout << "------------" << std::endl;
	return 0;
}
