#include "ShrubberyCreationForm.hpp"
#include <fstream>
#include <string>

ShrubberyCreationForm::ShrubberyCreationForm(const std::string &target)
	: Form("ShrubberyForm", 145, 137)
{
	this->target = target;
	std::cout << "ShrubberyCreationForm constructor called" << std::endl;
}

ShrubberyCreationForm::ShrubberyCreationForm()
	: Form("ShrubberyForm", 145, 137)
{
	target = "def";
	std::cout << "ShrubberyCreationForm constructor called" << std::endl;
}

ShrubberyCreationForm::ShrubberyCreationForm(const ShrubberyCreationForm &sfc)
	: Form(sfc.getName(), sfc.getSignGrade(), sfc.getExecGrade())
{
	target = sfc.target;
	std::cout << "ShrubberyCreationForm copy constructor called" << std::endl;
}

ShrubberyCreationForm &ShrubberyCreationForm::operator=(const ShrubberyCreationForm &sfc)
{
	std::cout << "ShrubberyCreationForm assigned called" << std::endl;
	target = sfc.target;
	return *this;
}

void ShrubberyCreationForm::execute() const
{
	std::ofstream	board(target + "_shrubbery", std::ios_base::out);
	if (!board.is_open())
		return;
	board <<
	"       ###\n"
	"      #o###\n"
	"    #####o###\n"
	"   #o#\\#|#/###\n"
	"    ###\\|/#o#\n"
	"     # }|{  #\n"
	"       }|{";
	board.close();
}

ShrubberyCreationForm::~ShrubberyCreationForm()
{
	std::cout << "ShrubberyCreationForm destructor called" << std::endl;
}
