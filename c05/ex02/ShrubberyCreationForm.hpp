#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP
#include <iostream>
#include "Form.hpp"

class ShrubberyCreationForm: public Form
{
public:
	ShrubberyCreationForm();
	ShrubberyCreationForm(const std::string &target);
	ShrubberyCreationForm(const ShrubberyCreationForm &);
	ShrubberyCreationForm&operator=(const ShrubberyCreationForm&);
	~ShrubberyCreationForm();

	void execute() const;
 
private:
	std::string target;
};
#endif
