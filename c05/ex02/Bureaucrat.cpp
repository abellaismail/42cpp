#include "Bureaucrat.hpp"
#include <ostream>
#include <stdexcept>
#include <string>
#include "Form.hpp"

Bureaucrat::Bureaucrat(): name("brr"), grade(150)
{
	std::cout << "Bureaucrat constructor called" << std::endl;
}

Bureaucrat::Bureaucrat(const std::string &name, int grade): name(name), grade(grade)
{
	std::cout << "Bureaucrat constructor called" << std::endl;
	if (grade < 1)
		throw Bureaucrat::GradeTooHighException();
	if (grade > 150)
		throw Bureaucrat::GradeTooLowException();
}

Bureaucrat::Bureaucrat(const Bureaucrat &br): name(br.getName()), grade(br.getGrade())
{
	std::cout << "Bureaucrat copy constructor called" << std::endl;
}

Bureaucrat &Bureaucrat::operator=(const Bureaucrat &br)
{
	std::cout << "Bureaucrat assigned called" << std::endl;
	grade = br.getGrade();
	return *this;
}

Bureaucrat::~Bureaucrat()
{
	std::cout << "Bureaucrat destructor called" << std::endl;
}

std::ostream &operator<<(std::ostream & os, Bureaucrat const& br)
{
	os << br.getName() << ",  bureaucrat grade " << br.getGrade() << std::endl ;
	return os;
}

const std::string &Bureaucrat::getName() const
{
	return name;
}

int Bureaucrat::getGrade() const{
	return grade;
}

Bureaucrat::GradeTooHighException::GradeTooHighException(): std::invalid_argument("Too High")
{}

Bureaucrat::GradeTooLowException::GradeTooLowException(): std::invalid_argument("Too Small")
{}

void Bureaucrat::inc()
{
	if (grade == 1)
		throw Bureaucrat::GradeTooHighException();
	grade--;
}

void Bureaucrat::dec()
{
	if (grade == 150)
		throw Bureaucrat::GradeTooLowException();
	grade++;
}

void Bureaucrat::signForm(Form &fr)
{
	try{
		fr.beSigned(*this);
	}catch(Form::GradeTooLowException &e)
	{
		std::cout << name << " couldn’t sign " << fr.getName() << " because "<< e.what()<< std::endl;	
	}
}

void Bureaucrat::executeForm(Form const & form)
{
	try {
		std::cout << name << " executed " << form.getName() << std::endl;
	}
	catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
	}
	form.execute_(*this);
}

