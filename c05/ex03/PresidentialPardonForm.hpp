#ifndef PRESIDENTIALPARDONFORM_HPP
# define PRESIDENTIALPARDONFORM_HPP
#include "Bureaucrat.hpp"
#include "Form.hpp"
#include <iostream>
#include <string>
class PresidentialPardonForm: public Form
{
public:
	static const std::string _name;
	PresidentialPardonForm(const std::string &target);
	PresidentialPardonForm();
	PresidentialPardonForm(const PresidentialPardonForm &);
	PresidentialPardonForm&operator=(const PresidentialPardonForm&);
	~PresidentialPardonForm();

	void execute() const;
	static Form *instance(const std::string &name, const std::string &target);
 
protected:
private:
	std::string target;

};
#endif
