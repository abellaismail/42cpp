#include "RobotomyRequestForm.hpp"
#include <string>

const std::string RobotomyRequestForm::_name = "robotomy request";

Form *RobotomyRequestForm::instance(const std::string &name, const std::string &target)
{
	if (name == _name)
		return new RobotomyRequestForm(target);
	return NULL;
}

RobotomyRequestForm::RobotomyRequestForm()
	: Form("Robotomy", 72, 45)
{
	target = "def";
	std::cout << "RobotomyRequestForm constructor called" << std::endl;
}

RobotomyRequestForm::RobotomyRequestForm(const std::string &target)
	: Form("Robotomy", 72, 45)
{
	this->target = target;
	std::cout << "RobotomyRequestForm constructor called" << std::endl;
}

RobotomyRequestForm::RobotomyRequestForm(const RobotomyRequestForm &rrf)
	: Form(rrf.getName(), rrf.getSignGrade(), rrf.getExecGrade())
{
	target = rrf.target;
	std::cout << "RobotomyRequestForm copy constructor called" << std::endl;
}

RobotomyRequestForm &RobotomyRequestForm::operator=(const RobotomyRequestForm &rrf)
{
	std::cout << "RobotomyRequestForm assigned called" << std::endl;
	target = rrf.target;
	return *this;
}

RobotomyRequestForm::~RobotomyRequestForm()
{
	std::cout << "RobotomyRequestForm destructor called" << std::endl;
}


void RobotomyRequestForm::execute() const{
	std::cout << "I am making some noise ...." << std::endl;
	std::cout << target << " has been robotomized successfully 50% of the tim\n";
}
