#include "Intern.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include <string>

Intern::Intern()
{
	std::cout << "Intern constructor called" << std::endl;
}

Intern::Intern(const Intern &it)
{
	std::cout << "Intern copy constructor called" << std::endl;
	(void) it;
}

Intern &Intern::operator=(const Intern &it)
{
	std::cout << "Intern assigned called" << std::endl;
	(void) it;
	return *this;
}

Intern::~Intern()
{
	std::cout << "Intern destructor called" << std::endl;
}

Form *Intern::makeForm(const std::string &form, const std::string &target)
{
	(void) target;
	Form *(*funcs[3])(const std::string &, const std::string &) = { &ShrubberyCreationForm::instance, &PresidentialPardonForm::instance, &RobotomyRequestForm::instance};
	int i = 0;
	Form *res;
	while(i < 3 && (res = funcs[i](form, target)) == NULL)
		i++;
	if (res ==  NULL)
		std::cout << "Form doesn't exists" << std::endl;
	return res;
}
