#ifndef SHRUBBERYCREATIONFORM_HPP
# define SHRUBBERYCREATIONFORM_HPP
#include <iostream>
#include "Form.hpp"

class ShrubberyCreationForm: public Form
{
public:
	static const std::string _name;
	static Form *instance(const std::string &name, const std::string &target);
	ShrubberyCreationForm();
	ShrubberyCreationForm(const std::string &target);
	ShrubberyCreationForm(ShrubberyCreationForm&);
	ShrubberyCreationForm(const ShrubberyCreationForm &);
	ShrubberyCreationForm&operator=(const ShrubberyCreationForm&);
	~ShrubberyCreationForm();

	void execute() const;
 
private:
	std::string target;
};
#endif
