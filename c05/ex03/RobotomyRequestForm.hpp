#ifndef ROBOTOMYREQUESTFORM_HPP
# define ROBOTOMYREQUESTFORM_HPP
#include "Form.hpp"
#include <iostream>
#include <string>
class RobotomyRequestForm: public Form
{
public:
	static const std::string _name;
	static Form *instance(const std::string &name, const std::string &target);
	RobotomyRequestForm();
	RobotomyRequestForm(const std::string &target);
	RobotomyRequestForm(RobotomyRequestForm&);
	RobotomyRequestForm(const RobotomyRequestForm &);
	RobotomyRequestForm&operator=(const RobotomyRequestForm&);
	~RobotomyRequestForm();
 
	void execute() const;
private:
	std::string target;

};
#endif
