#include "Bureaucrat.hpp"
#include "PresidentialPardonForm.hpp"
#include "RobotomyRequestForm.hpp"
#include "ShrubberyCreationForm.hpp"
#include "iostream"
#include "Form.hpp"
#include <exception>
#include "Intern.hpp"

int main() {

	Intern someRandomIntern;
	Form* rrf;
	Bureaucrat	b("b", 1);
	rrf = someRandomIntern.makeForm("robotomy request", "Home");
	//b.executeForm(*rrf);
	b.signForm(*rrf);
	b.executeForm(*rrf);



	if (rrf)
		delete rrf;
	return 0;
}
