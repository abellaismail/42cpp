#include "PresidentialPardonForm.hpp"
#include <string>

const std::string PresidentialPardonForm::_name = "PresidentialPardon";

Form *PresidentialPardonForm::instance(const std::string &name, const std::string &target)
{
	if (name == _name)
		return new PresidentialPardonForm(target);
	return NULL;
}

PresidentialPardonForm::PresidentialPardonForm()
	: Form("PresidentialPardonForm", 25, 5)
{
	target = "def";
	std::cout << "PresidentialPardonForm constructor called" << std::endl;
}

PresidentialPardonForm::PresidentialPardonForm(const std::string &target)
	: Form("PresidentialPardonForm", 25, 5)
{
	this->target = target;
	std::cout << "PresidentialPardonForm constructor called" << std::endl;
}

PresidentialPardonForm::PresidentialPardonForm(const PresidentialPardonForm &ppf)
{
	std::cout << "PresidentialPardonForm copy constructor called" << std::endl;
	target = ppf.target;
}

PresidentialPardonForm &PresidentialPardonForm::operator=(const PresidentialPardonForm &ppf)
{
	std::cout << "PresidentialPardonForm assigned called" << std::endl;
	target = ppf.target;
	return *this;
}

PresidentialPardonForm::~PresidentialPardonForm()
{
	std::cout << "PresidentialPardonForm destructor called" << std::endl;
}

void PresidentialPardonForm::execute() const
{
	std::cout << target << " has been pardoned by Zaphod Beeblebrox\n";
}

