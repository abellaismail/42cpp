#ifndef INTERN_HPP
# define INTERN_HPP
#include <iostream>
#include <string>
#include "Form.hpp"

class Intern
{
public:
	Intern();
	Intern(Intern&);
	Intern(const Intern &);
	Intern&operator=(const Intern&);
	~Intern();

	Form *makeForm(const std::string &form, const std::string &target);
 
protected:
private:

};
#endif
