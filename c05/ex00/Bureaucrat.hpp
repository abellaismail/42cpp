#ifndef BUREAUCRAT_HPP
# define BUREAUCRAT_HPP
#include <iostream>
#include <ostream>
#include <string>
class Bureaucrat
{
public:
	Bureaucrat();
	Bureaucrat(const std::string &name, int grade);
	Bureaucrat(const Bureaucrat &);
	Bureaucrat&operator=(const Bureaucrat&);
	~Bureaucrat();

	class GradeTooHighException: public std::invalid_argument {
	    public:
			GradeTooHighException();
	};

	class GradeTooLowException : public std::invalid_argument {
	    public:
			GradeTooLowException();
	};

	const std::string &getName() const;
	int getGrade() const;
	void inc();
	void dec();
 
protected:
private:
	const std::string name;
	int grade;

};


std::ostream &operator<<(std::ostream & os, const Bureaucrat&);
#endif
