#include "Bureaucrat.hpp"
#include "iostream"

int main() {
	Bureaucrat b("b1", 2);
	Bureaucrat b2("b2", 149);

	try {
		Bureaucrat b("Dead", -1);
	} catch (Bureaucrat::GradeTooLowException &e) {
		std::cout << e.what() << std::endl;
	} catch (Bureaucrat::GradeTooHighException &e) {
		std::cout << e.what() << std::endl;
	}

	b.inc();
	try {
		b.inc();
	} catch (Bureaucrat::GradeTooHighException &e) {
		std::cout << e.what() << std::endl;
	}
	b2.dec();
	try {
		b2.dec();
	}
	catch (Bureaucrat::GradeTooLowException &e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}
