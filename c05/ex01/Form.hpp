#ifndef FORM_HPP
# define FORM_HPP
#include <iostream>
#include <string>
#include "Bureaucrat.hpp"

class Form
{
public:
	Form();
	Form(const Form &);
	Form&operator=(const Form&);
	~Form();

	class GradeTooHighException: public std::invalid_argument {
	    public:
			GradeTooHighException();
	};

	class GradeTooLowException : public std::invalid_argument {
	    public:
			GradeTooLowException();
	};

	bool getSign() const;
	const std::string &getName() const;
	int getSignGrade() const;
	int getExecGrade() const;

	void beSigned(const Bureaucrat &b);
 
protected:
private:
	const std::string name;
	const int sign_grade;
	const int exec_grade;
	bool _signed;
};

std::ostream &operator<<(std::ostream & os, Form const& br);

#endif
