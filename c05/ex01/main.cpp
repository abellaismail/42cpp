#include "Bureaucrat.hpp"
#include "iostream"
#include "Form.hpp"

int main() {
	Bureaucrat b("b1", 1);
	Form f;

	try {
		b.signForm(f);
	} catch (Bureaucrat::GradeTooHighException &e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}
