#include "Form.hpp"

Form::Form(): name("Form"), sign_grade(2),exec_grade(1) , _signed(false)
{
	std::cout << "Form constructor called" << std::endl;
}

Form::Form(const Form &fr):
	name(fr.getName()),
	sign_grade(fr.getSignGrade()),
	exec_grade(fr.getExecGrade()),
	_signed(fr.getSign())
{
	std::cout << "Form copy constructor called" << std::endl;
}

Form &Form::operator=(const Form &fr)
{
	std::cout << "Form assigned called" << std::endl;
	_signed = fr.getSign();
	return *this;
}

void Form::beSigned(const Bureaucrat &b)
{
	if (b.getGrade() > sign_grade)
		throw Form::GradeTooLowException();
	std::cout << b.getName() << " signed " << name << std::endl;
	_signed = true;
}

Form::~Form()
{
	std::cout << "Form destructor called" << std::endl;
}

bool Form::getSign() const{ return _signed; }
const std::string &Form::getName() const { return name;}
int Form::getSignGrade() const { return sign_grade; }
int Form::getExecGrade() const { return exec_grade; }

Form::GradeTooHighException::GradeTooHighException(): std::invalid_argument("Too High")
{}

Form::GradeTooLowException::GradeTooLowException(): std::invalid_argument("Too Small")
{}

std::ostream &operator<<(std::ostream & os, Form const& fr)
{
	os << fr.getName()
		<< " Form ExecGrade( " << fr.getExecGrade()
		<< ") SignGrade " << fr.getSignGrade()
		<< "Signed " << fr.getSign()
		<< std::endl ;
	return os;
}
