#include "Cat.hpp"
#include "Animal.hpp"
#include "Brain.hpp"

Cat::Cat()
{
	std::cout << "Cat constructor called" << std::endl;
	type = "Cat";
	brain =  new Brain();
}

Cat::Cat(const Cat &cat): Animal()
{
	std::cout << "Cat copy constructor called" << std::endl;
	type = cat.type;
	brain = new Brain(*cat.brain);
}

Cat &Cat::operator=(const Cat &cat)
{
	std::cout << "Cat assigned called" << std::endl;
	type = cat.type;
	delete brain;
	brain = new Brain(*cat.brain);
	return *this;
}

void Cat::makeSound() const{
	std::cout << "miiiiiw" << std::endl;
}

Brain &Cat::getBrain()
{
	return *brain;
}

Cat::~Cat()
{
	delete brain;
	std::cout << "Cat destructor called" << std::endl;
}
