#include "WrongAnimal.hpp"

WrongAnimal::WrongAnimal()
{
	std::cout << "WrongAnimal constructor called" << std::endl;
}

WrongAnimal::WrongAnimal(const WrongAnimal &animal)
{
	std::cout << "WrongAnimal copy constructor called" << std::endl;
	type = animal.getType();
}

WrongAnimal &WrongAnimal::operator=(const WrongAnimal &animal)
{
	std::cout << "WrongAnimal assigned called" << std::endl;
	type = animal.getType();
	return *this;
}

void WrongAnimal::makeSound() const{
	std::cout << "whhh" << std::endl;
}

std::string WrongAnimal::getType() const{
	return type;
}

WrongAnimal::~WrongAnimal()
{
	std::cout << "WrongAnimal destructor called" << std::endl;
}
