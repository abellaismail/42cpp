#ifndef WANIMAL_HPP
# define WANIMAL_HPP

#include <iostream>
class WrongAnimal
{
public:
	WrongAnimal();
	WrongAnimal(const WrongAnimal &);
	WrongAnimal &operator=(const WrongAnimal &);
	std::string getType() const;
	void makeSound(void) const;
	virtual ~WrongAnimal();

protected:
	std::string type;
	
};

#endif
