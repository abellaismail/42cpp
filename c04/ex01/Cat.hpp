#ifndef CAT_HPP
# define CAT_HPP
#include "Animal.hpp"
#include "Brain.hpp"
#include <iostream>
class Cat: public Animal
{
public:
	Cat();
	Cat(const Cat &);
	Cat&operator=(const Cat&);
	~Cat();
	
	void makeSound() const;
	Brain &getBrain();

private:
	Brain *brain;

};
#endif
