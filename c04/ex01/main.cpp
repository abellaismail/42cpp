#include "Animal.hpp"
#include "Cat.hpp"
#include "Dog.hpp"
#include "WrongCat.hpp"
#include "WrongAnimal.hpp"

int _main()
{
	const Animal *animals[] = {new Dog(), new Cat(), new Dog(), new Cat()};
	
	for (int i = 0; i < 4; i++)
	{
		std::cout << "----------------" << std::endl;
		delete  animals[i];
	}
	
	Dog d;

	d.getBrain().setIdea(0, "hi1");

	Dog d1(d);
	std::cout << d1.getBrain().getIdea(0) << std::endl;

	return 0;
}

int main ()
{
	_main();
	return 0;
}
