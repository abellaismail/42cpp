#ifndef ANIMAL_HPP
# define ANIMAL_HPP

#include <iostream>
class Animal
{
public:
	Animal();
	Animal(const Animal &);
	Animal &operator=(const Animal &);
	std::string getType() const;
	virtual void makeSound(void) const;
	virtual ~Animal();

protected:
	std::string type;
	
};

#endif
