#ifndef BRAIN_HPP
# define BRAIN_HPP

#include <iostream>
class Brain
{
public:
	Brain();
	Brain(const Brain &);
	Brain &operator=(const Brain &);
	virtual ~Brain();

	void setIdea(int pos, const std::string &idea);
	std::string &getIdea(int pos);

protected:
	std::string ideas[100];

};

#endif
