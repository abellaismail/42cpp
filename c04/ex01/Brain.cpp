#include "Brain.hpp"
#include <string>

Brain::Brain()
{
	std::cout << "Brain constructor called#" << std::endl;
}

Brain::Brain(const Brain &br)
{
	std::cout << "Brain copy constructor called#" << std::endl;
	*this = br;
}

Brain &Brain::operator=(const Brain &br)
{
	std::cout << "Brain assigned called#" << std::endl;
	for (int i = 0; i < 100; i++){
		this->ideas[i] = br.ideas[i];
	}
	return *this;
}

void Brain::setIdea(int pos, const std::string &idea)
{
	ideas[pos] = idea;
}

std::string &Brain::getIdea(int pos)
{
	return ideas[pos];
}

Brain::~Brain()
{
	std::cout << "Brain destructor called#" << std::endl;
}
