#ifndef CHARACTER_HPP
# define CHARACTER_HPP
#include "ICharacter.hpp"
#include <iostream>
#include <string>
class Character: public ICharacter
{
public:
	Character();
	Character(std::string name);
	Character(const Character &);
	Character&operator=(const Character&);
	~Character();

	std::string const & getName() const;
	void equip(AMateria* m);
	void unequip(int idx);
	void use(int idx, ICharacter& target);
 
protected:
private:
	std::string name;
	AMateria *materias[4];
};
#endif
