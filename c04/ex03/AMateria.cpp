#include "AMateria.hpp"

AMateria::AMateria()
{
	std::cout << "AMateria constructor called" << std::endl;
}

AMateria::AMateria(std::string const &type)
{
	std::cout << "AMateria constructor called" << std::endl;
	this->type = type;
}

AMateria::AMateria(const AMateria &am)
{
	std::cout << "AMateria copy constructor called" << std::endl;
	this->type = am.type;
}

AMateria &AMateria::operator=(const AMateria &am)
{
	std::cout << "AMateria assigned called" << std::endl;
	this->type = am.type;
	return *this;
}

AMateria::~AMateria()
{
	std::cout << "AMateria destructor called" << std::endl;
}

std::string const & AMateria::getType() const
{
	return type;
}

void AMateria::use(ICharacter& target)
{
	std::cout << "Error: why are you calling me? =>" << target.getName() << std::endl;
}
