#ifndef MATERIASOURCE_HPP
# define MATERIASOURCE_HPP
#include <iostream>
#include "AMateria.hpp"
#include "IMateriaSource.hpp"

class MateriaSource: public IMateriaSource
{
public:
	MateriaSource();
	MateriaSource(MateriaSource&);
	MateriaSource(const MateriaSource &);
	MateriaSource&operator=(const MateriaSource&);
	~MateriaSource();
 
	void learnMateria(AMateria*);
	AMateria* createMateria(std::string const & type);
 
protected:
private:
	AMateria *materias[4];
	int index;

};
#endif
