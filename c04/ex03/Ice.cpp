#include "Ice.hpp"
#include "ICharacter.hpp"

Ice::Ice()
{
	std::cout << "Ice constructor called" << std::endl;
	type = "ice";
}

Ice::Ice(const Ice &ice): AMateria()
{
	std::cout << "Ice copy constructor called" << std::endl;
	this->type = ice.type;
}

Ice &Ice::operator=(const Ice &ice)
{
	std::cout << "Ice assigned called" << std::endl;
	this->type = ice.type;
	return *this;
}

Ice::~Ice()
{
	std::cout << "Ice destructor called" << std::endl;
}

AMateria* Ice::clone() const {
	Ice *ice = new Ice();
	ice->type = type;
	return ice;
}

void Ice::use(ICharacter& target)
{
	std::cout << "* heals " << target.getName() << "’s wounds *" << std::endl;
}
