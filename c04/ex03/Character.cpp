#include "Character.hpp"
#include "AMateria.hpp"

Character::Character()
{
	std::cout << "Character constructor called" << std::endl;
	for (int i = 0; i < 4; i++) {
		materias[i] = NULL;
	}
}

Character::Character(std::string name)
{
	std::cout << "Character constructor called" << std::endl;
	this->name = name;
	for (int i = 0; i < 4; i++) {
		materias[i] = NULL;
	}
}

Character::Character(const Character &ch)
{
	std::cout << "Character copy constructor called" << std::endl;
	for (int i = 0; i < 4; i++) {
		if (ch.materias[i])
			materias[i] = ch.materias[i]->clone();
		else
			materias[i] = NULL;
	}
	name = ch.name;
}

Character &Character::operator=(const Character &ch)
{
	std::cout << "Character assigned called" << std::endl;
	for (int i = 0; i < 4; i++)
	{
		delete materias[i];
		if (ch.materias[i])
			materias[i] = ch.materias[i]->clone();
		else
			materias[i] = NULL;
	}
	name = ch.name;
	return *this;
}

Character::~Character()
{
	for (int i = 0; i < 4; i++)
	{
		delete materias[i];
	}
	std::cout << "Character destructor called" << std::endl;
}

std::string const & Character::getName() const
{
	return name;
}

void Character::equip(AMateria* m)
{
	int i = 0;
	while (i < 4 && materias[i])
		i++;
	if (i == 4)
		return;
	this->materias[i] = m;
}

void Character::unequip(int idx){
	if (idx < 0 || idx >= 4 || materias[idx] ==  NULL)
		return;
	materias[idx] = NULL;
}

void Character::use(int idx, ICharacter& target){
	if (materias[idx] == NULL)
		return;
	AMateria *m = materias[idx];
	m->use(target);
}
