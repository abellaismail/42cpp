#include "MateriaSource.hpp"

MateriaSource::MateriaSource()
{
	std::cout << "MateriaSource constructor called" << std::endl;
	index = 0;
	for (int i = 0; i < 4; i++) {
		materias[i] = NULL;
	}
}

MateriaSource::MateriaSource(const MateriaSource &ms)
{
	std::cout << "MateriaSource copy constructor called" << std::endl;
	index = ms.index;
	for (int i = 0; i < index; i++) {
		if (ms.materias[i])
			materias[i] = ms.materias[i]->clone();
		else
			materias[i] = NULL;
	}
}

MateriaSource &MateriaSource::operator=(const MateriaSource &ms)
{
	std::cout << "MateriaSource assigned called" << std::endl;
	index = ms.index;
	int i;
	for (i = 0; i < 4; i++) {
		if (ms.materias[i])
			materias[i] = ms.materias[i]->clone();
		else
			materias[i] = NULL;
	}
	return *this;
}

MateriaSource::~MateriaSource()
{
	for (int i = 0; i < index; i++) {
		delete materias[i];
	}
	std::cout << "MateriaSource destructor called" << std::endl;
}

void MateriaSource::learnMateria(AMateria*m)
{
	if (index == 4)
		return;
	materias[index] = m;
	index++;
}

AMateria* MateriaSource::createMateria(std::string const & type)
{
	for (int i = 0; i < index; i++) {
		if (materias[i]->getType() == type)
			return materias[i]->clone();
	}
	return 0;
}
