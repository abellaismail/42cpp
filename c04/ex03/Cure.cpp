#include "Cure.hpp"
#include "ICharacter.hpp"

Cure::Cure()
{
	std::cout << "Cure constructor called" << std::endl;
	this->type = "cure";
}

Cure::Cure(const Cure &cure): AMateria()
{
	std::cout << "Cure copy constructor called" << std::endl;
	this->type = cure.type;
}

Cure &Cure::operator=(const Cure &cure)
{
	std::cout << "Cure assigned called" << std::endl;
	this->type = cure.type;
	return *this;
}

Cure::~Cure()
{
	std::cout << "Cure destructor called" << std::endl;
}

AMateria* Cure::clone() const {
	Cure *cure = new Cure();
	cure->type = type;
	return cure;
}

void Cure::use(ICharacter& target)
{
	std::cout << "* shoots an ice bolt at " << target.getName() << " *" << std::endl;
}
