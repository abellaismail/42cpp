#include "AMateria.hpp"
#include "IMateriaSource.hpp"
#include "ICharacter.hpp"
#include "MateriaSource.hpp"
#include "Cure.hpp"
#include "Character.hpp"
#include "Ice.hpp"


int _main (void)
{
	IMateriaSource* src = new MateriaSource();
	src->learnMateria(new Ice());
	src->learnMateria(new Cure());

	ICharacter* me = new Character("me");

	AMateria* tmp, *tmp1;
	tmp1 = src->createMateria("ice");
	tmp = src->createMateria("cure");

	me->equip(tmp);
	me->equip(tmp1);

	ICharacter* bob = new Character("bob");
	me->use(0, *bob);
	me->use(1, *bob);

	me->unequip(0);

	delete tmp;
	delete bob;
	delete me;
	delete src;
	return 0;
}
int main ()
{
	_main();
	return 0;
}
