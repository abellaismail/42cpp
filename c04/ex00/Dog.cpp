#include "Dog.hpp"

Dog::Dog()
{
	std::cout << "Dog constructor called" << std::endl;
	type = "Dog";
}

Dog::Dog(const Dog &dog): Animal()
{
	std::cout << "Dog copy constructor called" << std::endl;
	type = dog.type;
}

Dog &Dog::operator=(const Dog &dog)
{
	std::cout << "Dog assigned called" << std::endl;
	type = dog.type;
	return *this;
}

void Dog::makeSound() const{
	std::cout << "haw haw" << std::endl;
}

Dog::~Dog()
{
	std::cout << "Dog destructor called" << std::endl;
}
