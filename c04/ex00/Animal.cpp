#include "Animal.hpp"

Animal::Animal()
{
	std::cout << "Animal constructor called" << std::endl;
}

Animal::Animal(const Animal &animal)
{
	std::cout << "Animal copy constructor called" << std::endl;
	type = animal.getType();
}

Animal &Animal::operator=(const Animal &animal)
{
	std::cout << "Animal assigned called" << std::endl;
	type = animal.getType();
	return *this;
}

const std::string &Animal::getType() const {
	return type;
}

void Animal::makeSound(void) const{
	std::cout << "hhh" << std::endl;
}

Animal::~Animal()
{
	std::cout << "Animal destructor called" << std::endl;
}
