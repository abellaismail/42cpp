#include "Animal.hpp"
#include "Cat.hpp"
#include "Dog.hpp"
#include "WrongCat.hpp"
#include "WrongAnimal.hpp"

int main()
{
	const Animal* ani = new Animal();
	const Animal* dog = new Dog();
	const Animal* cat = new Cat();
	std::cout << "--------------" << std::endl;
	
	ani->makeSound();

	std::cout << dog->getType() << " ";
	dog->makeSound();

	std::cout << cat->getType() << " ";
	cat->makeSound();

	std::cout << "--------------" << std::endl;
	delete ani;
	delete cat;
	delete dog;

	std::cout << "--------------" << std::endl;
	const WrongAnimal* wani = new WrongAnimal();
	const WrongAnimal* wcat = new WrongCat();

	std::cout << "--------------" << std::endl;
	wani->makeSound();
	std::cout << wcat->getType() << ": ";
	wcat->makeSound();

	std::cout << "--------------" << std::endl;
	delete wani;
	delete wcat;

	return 0;
}
