#include "Cat.hpp"
#include "Animal.hpp"

Cat::Cat()
{
	std::cout << "Cat constructor called" << std::endl;
	type = "Cat";
}

Cat::Cat(const Cat &cat): Animal()
{
	std::cout << "Cat copy constructor called" << std::endl;
	*this = cat;
}

Cat &Cat::operator=(const Cat &cat)
{
	std::cout << "Cat assigned called" << std::endl;
	type = cat.type;
	return *this;
}

void Cat::makeSound() const{
	std::cout << "miiiiiw" << std::endl;
}

Cat::~Cat()
{
	std::cout << "Cat destructor called" << std::endl;
}
