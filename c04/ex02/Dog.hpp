#ifndef DOG_HPP
# define DOG_HPP
#include <iostream>
#include "Animal.hpp"
#include "Brain.hpp"

class Dog: public Animal
{
public:
	Dog();
	Dog(const Dog &);
	Dog&operator=(const Dog&);
	~Dog();

	void makeSound() const;
 
private:
	Brain *brain;

};
#endif
