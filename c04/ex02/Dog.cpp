#include "Dog.hpp"

Dog::Dog()
{
	std::cout << "Dog constructor called#" << std::endl;
	type = "Dog";
}

Dog::Dog(const Dog &dog): Animal()
{
	std::cout << "Dog copy constructor called#" << std::endl;
	type = dog.type;
	brain = new Brain(*dog.brain);
}

Dog &Dog::operator=(const Dog &dog)
{
	std::cout << "Dog assigned called#" << std::endl;
	type = dog.type;
	delete brain;
	brain = new Brain(*dog.brain);
	return *this;
}

void Dog::makeSound() const
{
	std::cout << "haw haw" << std::endl;
}

Dog::~Dog()
{
	std::cout << "Dog destructor called#" << std::endl;
}
