#include "WrongCat.hpp"
#include "WrongAnimal.hpp"

WrongCat::WrongCat()
{
	std::cout << "WrongCat constructor called" << std::endl;
	type = "Wrong Cat";
}

WrongCat::WrongCat(const WrongCat &wcat): WrongAnimal()
{
	std::cout << "WrongCat copy constructor called" << std::endl;
	*this = wcat;
}

WrongCat &WrongCat::operator=(const WrongCat &wcat)
{
	std::cout << "WrongCat assigned called" << std::endl;
	type = wcat.type;
	return *this;
}

void WrongCat::makeSound() const{
	std::cout << "miiiiiw" << std::endl;
}

WrongCat::~WrongCat()
{
	std::cout << "WrongCat destructor called" << std::endl;
}
