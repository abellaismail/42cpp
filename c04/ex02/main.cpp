#include "Animal.hpp"
#include "Cat.hpp"
#include "Dog.hpp"
#include "WrongCat.hpp"
#include "WrongAnimal.hpp"

int main()
{
	//Animal ani;
	const Animal *animals[] = {new Dog(), new Cat(), new Dog(), new Cat()};
	
	for (int i = 0; i < 4; i++) {
		delete  animals[i];
	}

	return 0;
}
