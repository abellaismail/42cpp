#include "Zombie.hpp"
#include <new>

int main ()
{
	Zombie *zombies = zombieHorde(10, "Foo");
	if (zombies == NULL)
		return 1;

	for (size_t i = 0; i < 10; i++)
		zombies[i].announce();
	
	for (size_t i = 0; i < 10; i++)
		zombies[i].~Zombie();
	
	::operator delete(zombies);
	return 0;
}
