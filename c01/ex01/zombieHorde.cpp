#include "Zombie.hpp"
#include <new>
#include <vector>

Zombie	*zombieHorde(int N, std::string name)
{
	Zombie* zombies = (Zombie *)::operator new(N * sizeof(Zombie), std::nothrow);

	if (zombies == NULL)
		return NULL;

	for (int i = 0; i < N; i++)
		new (zombies + i) Zombie(name);
	return zombies;
}
