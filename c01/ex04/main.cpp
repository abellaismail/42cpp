#include <algorithm>
#include <cstddef>
#include <iostream>
#include <fstream>
#include <ostream>
#include <string>
#include <sstream>

void replace_s(std::string &str, std::string &s1, std::string &s2)
{
	size_t pos;
	size_t cur = 0;
	while (cur < str.size())
	{
		pos = str.find(s1, cur);
		if (pos ==  std::string::npos)
				break;
		str.erase(pos, s1.size());
		str.insert(pos, s2);
		cur = pos + s2.size();
	}
}

bool ft_replace(std::string &filename, std::string &s1, std::string &s2)
{
	std::ifstream in(filename);
	std::ofstream out(filename + ".replace");

	if (in.fail())
		return false;
	if (out.fail())
	{
		in.close();
		return false;
	}

	std::stringstream buffer;
	buffer << in.rdbuf();

	std::string s(buffer.str());
	replace_s(s, s1, s2);
	out << s;
	in.close();
	out.close();
	return true;
}

int main (int ac, char *av[])
{
	if (ac == 4)
	{
		std::string filename(av[1]);
		std::string s1(av[2]);
		std::string s2(av[3]);
		if(s1.empty())
		{
			std::cerr << "s1 cannot be empty" << std::endl;
			return 1;
		}

		if (!ft_replace(filename, s1, s2))
			return 1;
	}
	else
	{
		std::cerr << "./sedpp filename s1 s2" << std::endl;
		return 1;
	}
	return 0;
}
