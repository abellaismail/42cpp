#ifndef WEAPON_HPP
#define WEAPON_HPP


#include <string>
class Weapon
{
public:
	Weapon(const std::string &type);
	void setType(const std::string &type);
	const std::string &getType();
	~Weapon();

private:
	Weapon();
	std::string type;
	
};

#endif
