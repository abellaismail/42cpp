#ifndef HUMAN_A_HPP
#define HUMAN_A_HPP

#include <string>

class Weapon;

class HumanA
{
public:
	HumanA(const std::string, Weapon &);
	void setWeapon(const Weapon &);
	const Weapon &getWeapon();
	void setName(const std::string &);
	const std::string &getName(std::string &);
	void attack(void);
	~HumanA();

private:
	HumanA();
	Weapon &weapon;
	std::string name;
	
};

#endif
