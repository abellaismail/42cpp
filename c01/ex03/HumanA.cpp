#include "HumanA.hpp"
#include "Weapon.hpp"
#include <iostream>

HumanA::HumanA(const std::string name, Weapon &weapon) : weapon(weapon)
{
	this->weapon = weapon;
	this->name = name;
}

void HumanA::setWeapon(const Weapon &weapon)
{
	this->weapon = weapon;
}
const Weapon &HumanA::getWeapon()
{
	return weapon;
}
void HumanA::setName(const std::string &name)
{
	this->name = name;
}
const std::string &HumanA::getName(std::string &name)
{
	return name;
}
void HumanA::attack(void)
{
	std::cout << name << " attacks with their " << weapon.getType() << std::endl;
}

HumanA::~HumanA()
{
	std::cout << name << ": HumanA destroyed" << std::endl;
}
