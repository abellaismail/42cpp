#ifndef HUMAN_B_HPP
#define HUMAN_B_HPP

#include <string>

class Weapon;

class HumanB
{
public:
	HumanB(const std::string name);
	void setWeapon(Weapon &weapon);
	const Weapon *getWeapon();
	void setName(const std::string &name);
	const std::string &getName();
	void attack(void);
	~HumanB();

private:
	Weapon *weapon;
	std::string name;
	
};

#endif
