#include "Weapon.hpp"
#include <string>
#include <iostream>

Weapon::Weapon(const std::string &type)
{
	setType(type);
}
void Weapon::setType(const std::string &type)
{
	this->type = type;
}
const std::string &Weapon::getType()
{
	return type;
}

Weapon::~Weapon(){
	std::cout << "Weapon destroyed" << std::endl;
}
