#include "HumanB.hpp"
#include "Weapon.hpp"
#include <iostream>
#include <string>

using std::cout;

HumanB::HumanB(const std::string name) {
  setName(name);
  this->weapon = NULL;
}

void HumanB::setWeapon(Weapon &weapon)
{
	this->weapon = &weapon;
}
const Weapon *HumanB::getWeapon()
{
	return weapon;
}
void HumanB::setName(const std::string &name)
{
	this->name = name;
}
const std::string &HumanB::getName()
{
	return this->name;
}
void HumanB::attack(void)
{
	if (weapon == NULL)
		return;
        cout << name << " attacks with their " << weapon->getType()
             << std::endl;
}

HumanB::~HumanB()
{
	std::cout << name << ": HumanB destroyed" << std::endl;
}
