#include "Harl.hpp"
#include <string>
#include <vector>


Harl::Harl()
{
}

void Harl::debug(void)
{
	std::cout << "[ DEBUG ]\nI have 5 Cups\n" << std::endl;
}

void Harl::info(void)
{
	std::cout << "[ INFO ]\nit's raining out there\n" << std::endl;

}
void Harl::warning(void)
{
	std::cout << "[ WARNING ]\nYou are doing a lot of work\n" << std::endl;
}
void Harl::error(void)
{
	std::cout << "[ ERROR ]\nI can't open it\n" << std::endl;
}

void Harl::snagat(void)
{
	std::cout << "[ Probably complaining about insignificant problems ]" << std::endl;
}

void Harl::complain(std::string level)
{
	std::string keys[] = {"DEBUG", "INFO", "WARNING", "ERROR"};

	int i = 0;
	while (keys[i] != level && i < 4)
		i++;
	switch (i) {
		case 0:
			debug();
		case 1:
			info();
		case 2:
			warning();
		case 3:
			error();
			break;
		default:
			snagat();
			break;
	}
}


Harl::~Harl()
{
}
