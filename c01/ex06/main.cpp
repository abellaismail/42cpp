#include "Harl.hpp"


int main (int ac, char *av[])
{
	Harl harl = Harl();
	if (ac == 2)
	{
		harl.complain(av[1]);
	}
	else
	{
		std::cout << "./HarlFilter LogLevel"  << std::endl;
		return 1;
	}
	return 0;
}
