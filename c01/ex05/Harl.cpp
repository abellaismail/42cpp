#include "Harl.hpp"
#include <string>
#include <vector>


Harl::Harl()
{
}

void Harl::debug(void)
{
	std::cout << "DEBUG: I have 5 Cups" << std::endl;
}

void Harl::info(void)
{
	std::cout << "INFO: it's raining out there" << std::endl;

}
void Harl::warning(void)
{
	std::cout << "WARN: You are doing a lot of work" << std::endl;
}
void Harl::error(void)
{
	std::cout << "ERROR: I can't open it" << std::endl;
}

void Harl::snagat(void)
{
	//useless function
}

void Harl::complain(std::string level)
{
	void (Harl::*funcs[5])(void) = { &Harl::debug, &Harl::info, &Harl::warning, &Harl::error, &Harl::snagat};
	std::string keys[] = {"DEBUG", "INFO", "WARNING", "ERROR"};

	int i = 0;
	while (keys[i] != level && i < 4)
		i++;
	(this->*funcs[i])();
}


Harl::~Harl()
{
}
