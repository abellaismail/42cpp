#ifndef TEMPLATE_HPP
# define TEMPLATE_HPP


template<typename T>
void	swap(T &a, T &b) {
	T tmp = a;
	a = b;
	b = tmp;
}

template <typename T>
T const	&min(T &a, T &b) {
	if (a < b)
		return (a);
	return (b);
}

template <typename T>
T const	&max(T &a, T &b) {
	if (a > b)
		return (a);
	return (b);
}

#endif
