#include "template.hpp"
#include "iostream"

int	main(){
	int a = 1337;
	int b = 42;

	swap(a, b);

	std::cout << "a = " << a << ", b = " << b << std::endl;
	std::cout << "min[a, b] =" << min( a, b ) << std::endl;
	std::cout << "max[a, b] =" << max( a, b ) << std::endl;

	std::string c = "cpp";
	std::string d = "rust";

	std::cout << "before swap : c = " << c << ", d = " << d << std::endl;
	swap(c, d);
	std::cout << "after swap : c = " << c << ", d = " << d << std::endl;
	return 0;
}
