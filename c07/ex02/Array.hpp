#ifndef ARRAY_HPP
# define ARRAY_HPP

#include <cstddef>
#include <stdexcept>
#include <strings.h>


template<typename T>
class Array
{
public:
	Array(): arr(0), size(0) { }

	Array(unsigned int n) {
		arr = new T[n];
		bzero(arr, n * sizeof(T));
		size = n;
	}

	Array(Array<T> const &narr){
		size = narr.size;
		arr = new T[size];
		for (unsigned int i = 0; i < size; i++)
			arr[i] = narr.arr[i];
	}

	Array<T> &operator=(Array<T> const &arr)
	{
		for (unsigned int i = 0; i < size && i < arr.size; i++)
			arr[i] = arr.arr[i];
		return *this;
	}

	T &operator[](unsigned int index)
	{
		if (index >= size)
			throw std::out_of_range("this index is out of bounds");
		return (arr[index]);
	};

	unsigned int	getSize() const{
		return size;
	};

	~Array(){};

private:
	T *arr;
	unsigned int size;
	
};

#endif
