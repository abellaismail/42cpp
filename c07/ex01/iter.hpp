#ifndef ITER_HPP
# define ITER_HPP

#include <cstddef>
#include <iostream>

template<typename T>
void	iter(T *array, size_t length, void (*f)(T &val))
{
	for (size_t i = 0; i < length; i++)
		f(array[i]);
}

template<typename T>
void	iter(T const *array, size_t const length, void (*f)(T const &val)) {
	for (size_t i = 0; i < length; i++)
		f(array[i]);
}

#endif
