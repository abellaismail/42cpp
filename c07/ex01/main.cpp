#include "iter.hpp"
#include "iostream"
#include <cstring>
#include "stdio.h"


void println(const char &c)
{
	std::cout << c << std::endl;
}

int	main(){
	char ch1[5]="AbCd";     //(\0)=NULL
	char *s = ch1;

	iter(ch1, std::strlen(s), println);
	return 0;
}
