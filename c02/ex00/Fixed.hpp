#ifndef FIXED_POINT_HPP
#define FIXED_POINT_HPP
#include <iostream>

class Fixed
{
public:
	Fixed();
	Fixed &operator=(const Fixed &f);
	Fixed(const Fixed &f);
	~Fixed();

	void setRawBits(int const raw);
	int getRawBits(void) const;


private:
	int value;
	static const int position = 8;
};

#endif
