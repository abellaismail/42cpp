#ifndef POINT_HPP
# define POINT_HPP

#include "Fixed.hpp"

class Point
{
public:
	Point();
	Point(const float x, const float y);
	Point(const Point &);
	Point &operator=(const Point &);
	~Point();

	Fixed gX() const;
	Fixed gY() const;

private:
	const Fixed x;
	const Fixed y;
	
};
#endif
