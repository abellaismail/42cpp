#include "Point.hpp"
#include "Fixed.hpp"
#include <iostream>

Point::Point() :x(0.0f), y(0.0f)
{
	std::cout << "Point constructor" << std::endl;
}

Point::Point(const float x, const float y) :x(x), y(y)
{
	std::cout << "Point constructor" << std::endl;
}

Point::Point(const Point &p): x(p.x), y(p.y)
{
	std::cout << "Point copy constructor\r" << std::endl;
}

Fixed Point::gY() const
{
	return this->y;
}

Fixed Point::gX() const
{
	return this->x;
}

Point &Point::operator=(const Point &p)
{
	(void) p;
	std::cout << "Useless copy assignment op" << std::endl;
	return *this;
}
Point::~Point()
{
	std::cout << "Point destructor" << std::endl;
}
