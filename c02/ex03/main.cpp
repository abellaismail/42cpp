#include "Point.hpp"
#include "Fixed.hpp"
#include <ostream>

bool bsp(Point const a, Point const b, Point const c, Point const point);

int main ()
{
	Point a(2, 4);
	Point b(2, 0);
	Point c(-4, 0);
	std::cout << bsp(a,b, c, Point(1, 1)) << std::endl;
	std::cout << bsp(a,b, c, Point(0.2f, 0.01f)) << std::endl;
	return 0;
}
