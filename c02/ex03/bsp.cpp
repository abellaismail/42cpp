#include "Fixed.hpp"
#include "Point.hpp"
#include <cmath>
#include <math.h>

Fixed cross_pdc(Point const &a, Point const &b, Point const &p)
{
	return (( a.gX() - p.gX()) * (b.gY() - p.gY())
			- (b.gX() - p.gX()) * (a.gY() - p.gY())
			);
}

bool bsp(Point const a, Point const b, Point const c, Point const point)
{
	Fixed area1 = cross_pdc(point,b,c);
	Fixed area2 = cross_pdc(point,a,b);
	Fixed area3 = cross_pdc(point,c,a);

	if (area1 * area2 * area3 == 0)
		return 0;

	if ( (area1 > Fixed(0) && area2 > Fixed(0) && area3 > 0)
		|| (area1 < Fixed(0) && area2 < Fixed(0) && area3 < 0))
		return 1;

	return 0;
}
