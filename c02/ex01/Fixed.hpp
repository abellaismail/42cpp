#ifndef FIXED_HPP
#define FIXED_HPP
#include <iostream>
#include <ostream>

class Fixed
{
public:
	Fixed();
	Fixed(const int);
	Fixed(const float);
	Fixed &operator=(const Fixed &);
	Fixed(const Fixed &);
	~Fixed();

	float toFloat() const;
	int toInt() const;
	void setRawBits(int const);
	int getRawBits(void) const;


private:
	int value;
	static const int position = 8;
};

std::ostream &operator<<(std::ostream &os, const Fixed &fp);

#endif
