#include "Fixed.hpp"
#include <cstring>
#include <ostream>
#include <stdio.h>
#include <cmath>

Fixed::Fixed(): value(0)
{
	std::cout << "Default constructor called" << std::endl;
}

Fixed::Fixed(const int val)
{
	std::cout << "Int constructor called" << std::endl;
	value = val << position;
}

Fixed::Fixed(const float f)
{
	std::cout << "Float constructor called" << std::endl;
	value = std::roundf(f * (1 << position));
}

Fixed::Fixed(const Fixed &f)
{
	std::cout << "Copy constructor called" << std::endl;
	*this = f;
}

Fixed &Fixed::operator=(const Fixed &f)
{
	std::cout << "Copy assignment operator called" << std::endl;
	this->value = f.getRawBits();
	return *this;
}
std::ostream &operator<<(std::ostream &os, const Fixed &fp)
{
	os << fp.toFloat();
	return os;
}

int Fixed::toInt() const
{
	return value >> position;
}

float Fixed::toFloat() const
{
	return (float) value / (1 << position);
}

bool Fixed::operator==(const Fixed &fp) const
{
	return value == fp.value;
}

bool Fixed::operator!=(const Fixed &fp) const
{
	return value != fp.value;
}

bool Fixed::operator>=(const Fixed &fp) const
{
	return value >= fp.value;
}

bool Fixed::operator<=(const Fixed &fp) const
{
	return value <= fp.value;
}

bool Fixed::operator>(const Fixed &fp) const
{
	return value > fp.value;
}

bool Fixed::operator<(const Fixed &fp) const
{
	return value < fp.value;
}

Fixed Fixed::operator+(const Fixed &fp)
{
	Fixed nfp;
	nfp.value = value + fp.value;
	return nfp;
}

Fixed Fixed::operator-(const Fixed &fp)
{
	Fixed nfp;
	nfp.value = value - fp.value;
	return nfp;
}

Fixed Fixed::operator*(const Fixed &fp)
{
	Fixed nfp;
	nfp.setRawBits(
			((int64_t)value * (int64_t)fp.value)
			/ (1 << position));
	return nfp;
}

Fixed Fixed::operator/(const Fixed &fp)
{
	Fixed nfp;
	nfp.setRawBits(
	((int64_t) value  * (1 << position)) / fp.getRawBits());
	return nfp;
}

std::ostream &operator-(std::ostream &os, const Fixed &fp)
{
	os << fp.toFloat();
	return os;
}

Fixed &Fixed::operator++()
{
	value++;
	return *this;
}

Fixed Fixed::operator++(int)
{
	Fixed copy(*this);
	value++;
	return copy;
}

Fixed &Fixed::operator--()
{
	value--;
	return *this;
}

Fixed Fixed::operator--(int)
{
	Fixed copy(*this);
	value--;
	return copy;
}

int Fixed::getRawBits(void) const
{
	std::cout << "getRawBits member function called" << std::endl;
	return value;
}

void Fixed::setRawBits(int const raw)
{
	std::cout << "setRawBits member function called" << std::endl;
	value = raw;
}

Fixed &Fixed::min(Fixed& a, Fixed& b)
{
	if (a < b)
		return a;
	return b;
}

Fixed &Fixed::max(Fixed& a, Fixed& b)
{
	if (a > b)
		return a;
	return b;
}

const Fixed &Fixed::min(const Fixed& a, const Fixed& b)
{
	if (a < b)
		return a;
	return b;
}

const Fixed &Fixed::max(const Fixed& a, const Fixed& b)
{
	if (a > b)
		return a;
	return b;
}

Fixed::~Fixed()
{
	std::cout << "Destructor called" << std::endl;
}
