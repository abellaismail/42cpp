#ifndef FIXED_HPP
#define FIXED_HPP
#include <iostream>
#include <ostream>

class Fixed
{
public:
	Fixed();
	Fixed(const int);
	Fixed(const float);
	Fixed &operator=(const Fixed &);
	Fixed(const Fixed &);
	bool operator==(const Fixed &fp) const;
	bool operator!=(const Fixed &fp) const;
	bool operator<=(const Fixed &) const;
	bool operator>=(const Fixed &) const;
	bool operator>(const Fixed &) const;
	bool operator<(const Fixed &) const;
	Fixed operator+(const Fixed &);
	Fixed operator-(const Fixed &);
	Fixed operator*(const Fixed &);
	Fixed operator/(const Fixed &);
	Fixed &operator++();
	Fixed operator++(int);
	Fixed &operator--();
	Fixed operator--(int);
	static Fixed &min(Fixed&, Fixed&);
	static Fixed &max(Fixed&, Fixed&);
	static const Fixed &min(const Fixed&, const Fixed&);
	static const Fixed &max(const Fixed&, const Fixed&);
	float toFloat() const;
	int toInt() const;
	void setRawBits(int const);
	int getRawBits(void) const;

	~Fixed();

private:
	int value;
	static const int position = 8;
};

std::ostream &operator<<(std::ostream &os, const Fixed &fp);

#endif
