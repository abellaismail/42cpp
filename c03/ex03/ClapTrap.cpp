#include "ClapTrap.hpp"

ClapTrap::ClapTrap() : name("default clap"), health(10), energy(10), damage(0)
{
	std::cout << "calpTrap constructor called#" << std::endl;
}

ClapTrap::ClapTrap(std::string name) : health(10), energy(10), damage(0)
{
	std::cout << "calpTrap(" <<  name << ") constructor called#" << std::endl;
	this->name = name;
}

ClapTrap::ClapTrap(const ClapTrap &ct)
{
	std::cout << "calpTrap copy constructor called#" << std::endl;
	*this = ct;
}

ClapTrap &ClapTrap::operator=(const ClapTrap &ct)
{
	std::cout << "calpTrap = called#" << std::endl;
	this->health = ct.health;
	this->name = ct.name;
	this->energy = ct.energy;
	this->damage = ct.damage;
	return *this;
}

void ClapTrap::attack(const std::string &target)
{
	if (energy == 0 || health == 0)
		return;
	std::cout << "ClapTrap " << name << " attacks " << target << " , causing " << damage << " points of damage!" << std::endl;
	energy--;
}

void ClapTrap::takeDamage(unsigned int amount)
{
	if (energy == 0 || health == 0)
		return;
	std::cout << "ClapTrap " << name << " takes damage -" << amount << " hit points"  << std::endl;
	if (health < amount)
		health = 0;
	else
		health -= amount;
}

void ClapTrap::toRepaired(unsigned int amount)
{
	if (energy == 0 || health == 0)
		return;
	energy--;
	std::cout << "ClapTrap " << name << " repaired"  << std::endl;
	if (health + amount < health)
		health = ~0;
	else
		health += amount;
}

ClapTrap::~ClapTrap()
{
	std::cout << "calpTrap(" <<  name << ") destructor called#" << std::endl;
}

