#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include "ClapTrap.hpp"
# include "ScavTrap.hpp"

class FragTrap: public virtual ClapTrap{
    public :
        FragTrap(std::string name);
        FragTrap();
        FragTrap(const FragTrap &scav);
		~FragTrap();
        FragTrap    &operator=(const FragTrap &scav);
        void        highFivesGuys();
};

#endif
