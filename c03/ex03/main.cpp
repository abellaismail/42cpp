#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"
#include "DiamondTrap.hpp"

int	main() {
	DiamondTrap	cl1("Diamond");
	std::cout << "-----------" << std::endl;

	cl1.guardGate();
	cl1.highFivesGuys();
	cl1.attack("Frag");
	cl1.takeDamage(1);

	std::cout << "-----------" << std::endl;
	std::cout << cl1.getClapTrapName() << std::endl;
	std::cout << cl1.getDiamondName() << std::endl;
	cl1.whoAmI();
	
	std::cout << "-----------" << std::endl;
	return (0);
}
