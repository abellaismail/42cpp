#include "ScavTrap.hpp"
#include "ClapTrap.hpp"

ScavTrap::ScavTrap()
{
	std::cout << "ScavTrap constructor called#" << std::endl;
	this->health = 100;
	this->energy = 50;
	this->damage = 20;
}

ScavTrap::ScavTrap(std::string name)
{
	std::cout << "ScavTrap constructor called#" << std::endl;
	this->name = name;
	this->health = 100;
	this->energy = 50;
	this->damage = 20;
}

ScavTrap::ScavTrap(const ScavTrap &st): ClapTrap(st.name){
	std::cout << "ScavTrap copy constructor called#" << std::endl;
    *this = st;
}

ScavTrap    &ScavTrap::operator=(const ScavTrap &st){
	std::cout << "ScavTrap = called#" << std::endl;
	this->name = st.name;
	this->health = st.health;
	this->energy = st.energy;
	this->damage = st.damage;
    return (*this);
}

ScavTrap::~ScavTrap(){
    std::cout << "ScavTrap destructor called#" << std::endl;
}

void        ScavTrap::guardGate(){
    std::cout << "I am now in Gate keeper mode" << std::endl;
}

void    ScavTrap::attack(const std::string &targate){
	if (energy == 0){
		return ;
	}
    std::cout << "ScavTrap " << name << " attacks " << targate << ", causing 1 points of damage!" << std::endl;
	energy--;
}
