#ifndef DIAMONDTRAP_HPP
# define DIAMONDTRAP_HPP

#include "ScavTrap.hpp"
#include "FragTrap.hpp"

class DiamondTrap : public FragTrap, public ScavTrap
{
	private:
		std::string name;
	
	public:
		DiamondTrap();
		DiamondTrap(DiamondTrap const &diamond);
		DiamondTrap		&operator=(const DiamondTrap &diamond);
		DiamondTrap(const std::string &name);
		~DiamondTrap();
		std::string		getClapTrapName(void);
		std::string		getDiamondName(void);
		void			attack( const std::string &target );
		void			whoAmI( void );
};


#endif
