#include "DiamondTrap.hpp"

DiamondTrap::DiamondTrap(){
	std::cout << "Diamond default constructor called" << std::endl;
	this->health = FragTrap::health;
	this->energy = ScavTrap::energy;
	this->damage = FragTrap::damage;
}

DiamondTrap::DiamondTrap(const std::string &name){
	std::cout << "Diamond constructor called" << std::endl;
	DiamondTrap::name = name;
	ClapTrap::name = name + "_clap_name";
	this->health = FragTrap::health;
	this->energy = ScavTrap::energy;
	this->damage = FragTrap::damage;
}

DiamondTrap::DiamondTrap(const DiamondTrap &diamond){
	std::cout << "Diamond copy constructor called" << std::endl;
	this->health = diamond.health;
	this->damage = diamond.damage;
	this->energy = diamond.energy;
	this->name = diamond.name;
	ClapTrap::name = this->name + "_clap_name";
}

DiamondTrap::~DiamondTrap(){
	std::cout << "Diamond destructor called" << std::endl;
}

DiamondTrap	&DiamondTrap::operator=(const DiamondTrap &diamond){
	this->name = diamond.name;
	this->damage = diamond.damage;
	this->energy = diamond.energy;
	this->health = diamond.health;
	ClapTrap::name = this->name + "_clap_name";
	return (*this);
}

void	DiamondTrap::attack(const std::string &target){
	ScavTrap::attack(target);
}

std::string	DiamondTrap::getClapTrapName(){
	return (ClapTrap::name);
}

std::string	DiamondTrap::getDiamondName(){
	return (DiamondTrap::name);
}

void	DiamondTrap::whoAmI(){
	std::cout << "name : " << DiamondTrap::name << '\n' << "name of clap trap: " << ClapTrap::name << std::endl;
}
