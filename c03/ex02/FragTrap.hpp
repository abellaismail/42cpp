#ifndef FRAGTRAP_HPP
# define FRAGTRAP_HPP

# include "ClapTrap.hpp"
# include "ScavTrap.hpp"


class FragTrap: public ClapTrap{
    public :
        FragTrap();
        FragTrap(std::string name);
        FragTrap(const FragTrap &scav);
        FragTrap    &operator=(const FragTrap &scav);
		~FragTrap();

        void        highFivesGuys();
};

#endif
