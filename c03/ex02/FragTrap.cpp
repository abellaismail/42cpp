#include "FragTrap.hpp"
#include "ClapTrap.hpp"

FragTrap::FragTrap()
{
	std::cout << "FragTrap constructor called#" << std::endl;
	this->health = 100;
	this->energy = 100;
	this->damage = 30;
}

FragTrap::FragTrap(std::string name)
{
	std::cout << "FragTrap constructor called#" << std::endl;
	this->name = name;
	this->health = 100;
	this->energy = 100;
	this->damage = 30;
}

FragTrap::FragTrap(const FragTrap &st): ClapTrap(st.name){
	std::cout << "FragTrap copy constructor called#" << std::endl;
    *this = st;
}

FragTrap &FragTrap::operator=(const FragTrap &st){
	std::cout << "FragTrap assign operator called#" << std::endl;
	this->name = st.name;
	this->health = st.health;
	this->energy = st.energy;
	this->damage = st.damage;
    return (*this);
}

FragTrap::~FragTrap(){
    std::cout << "FragTrap destructor called#" << std::endl;
}

void FragTrap::highFivesGuys(){
    std::cout << "positive high fives" << std::endl;
}
