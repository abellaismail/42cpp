#include "ClapTrap.hpp"
#include "ScavTrap.hpp"
#include "FragTrap.hpp"

int	main() {
	ClapTrap	cl1("Clap1");
	FragTrap cl2("Frag");
	std::cout << "-----------" << std::endl;

	cl1.attack("Frag");
	cl2.takeDamage(100);
	cl2.attack("CLAP1");
	cl1.takeDamage(100);
	cl2.highFivesGuys();

	std::cout << "-----------" << std::endl;
	return (0);
}
