#include "ClapTrap.hpp"
#include "ScavTrap.hpp"

int	main() {
	ClapTrap	cl1("Clap1");
	ScavTrap cl2("Scav2");
	std::cout << "-----------" << std::endl;

	cl1.attack("CLAP2");
	cl2.takeDamage(100);
	cl2.attack("CLAP1");
	cl1.takeDamage(100);
	cl2.guardGate();

	std::cout << "-----------" << std::endl;

	cl2.takeDamage(9);
	for (int i = 10; i; i--)
		cl2.attack("CLAP1");

	std::cout << "-----------" << std::endl;
	return (0);
}
