#ifndef CLAPTRAP_HPP
# define CLAPTRAP_HPP

#include <iostream>

class ClapTrap
{
public:
	ClapTrap();
	ClapTrap(std::string name);
	ClapTrap(const ClapTrap &);
	ClapTrap &operator=(const ClapTrap &);
	~ClapTrap();

	void attack(const std::string &target);
	void takeDamage(unsigned int amount);
	void toRepaired(unsigned int amount);

private:
	std::string name;
	unsigned int health;
	unsigned int energy;
	unsigned int damage;
};

#endif
