#include "ClapTrap.hpp"

int	main() {
	ClapTrap	cl1("CLAP1");
	ClapTrap	cl2("CLAP2");

	cl1.attack("CLAP2");
	cl2.takeDamage(100);
	cl2.attack("CLAP1");
	cl1.takeDamage(100);
	cl2.toRepaired(100);

	std::cout << "-----------" << std::endl;

	cl2.takeDamage(9);
	for (int i = 10; i; i--)
		cl2.attack("CLAP1");

	return (0);
}

