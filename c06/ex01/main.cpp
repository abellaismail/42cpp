#include "serialize.h"
#include "Data.hpp"


uintptr_t serialize(Data* ptr)
{
	return reinterpret_cast<uintptr_t>(ptr);
}

Data *deserialize(uintptr_t raw)
{
	return reinterpret_cast<Data *>(raw);
}

int main ()
{
	Data d(1, "ismail");

	uintptr_t v = serialize(&d);
	Data *d_ptr = deserialize(v);

	std::cout << d_ptr->s << d_ptr->num << std::endl;
	return 0;
}
