#ifndef SERIALIZE_H
# define SERIALIZE_H

# include <iostream>
# include <cstdlib>
# include <stdint.h>
#include  "Data.hpp"

uintptr_t	serialize(Data* ptr);
Data		*deserialize(uintptr_t raw);

#endif
