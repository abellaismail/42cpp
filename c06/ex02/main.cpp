#include "A.hpp"
#include "B.hpp"
#include "C.hpp"
#include "Base.hpp"
#include <cstdlib>

template<typename T>
bool is(Base *p){
	return dynamic_cast<T *>(p) != NULL;
}

template<typename T>
bool is(Base &p){
	try {
		(void) dynamic_cast<T &>(p);
		return true;
	}
	catch (const std::exception&) {
		return false;
	}
}

Base *generate()
{
	srand(time(0));
	int r = rand() % 3;

	if (r == 2)
		return new A();
	else if (r == 1)
		return new B();
	else
		return new C();
}

void identify(Base *p)
{
	if (is<A>(p))
		std::cout << "A" << std::endl;
	if (is<B>(p))
		std::cout << "B" << std::endl;
	if (is<C>(p))
		std::cout << "C" <<std::endl;
}

void identify(Base &p)
{
	if (is<A>(p))
		std::cout << "A" << std::endl;
	if (is<B>(p))
		std::cout << "B" << std::endl;
	if (is<C>(p))
		std::cout << "C" <<std::endl;
}

int main ()
{
	Base *base = generate();
	Base &ref = *base;
	
	identify(base);
	identify(ref);
	
	delete base;
	return 0;
}
