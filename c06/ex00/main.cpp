#include <iostream>
#include <iomanip>
#include <limits>
#include <limits.h>
#include <cstdlib>

bool is_printable(int c)
{
	return (c >= 32 && c <= 126);
}

void printInt(int i)
{
	std::cout << "char: ";
	if (i > 127 || i < 0)
		std::cout << "Impossible" << std::endl;
	else if (!is_printable(i))
		std::cout << "Non displayable" << std::endl;
	else
		std::cout << static_cast<int>(i) << std::endl;
	std::cout << "int: " << i << std::endl;
	std::cout << "float: " << i << ".0f" << std::endl;
	std::cout << "double: " << i << ".0" << std::endl;
}

void printDouble(double d, bool is_nan)
{
	if (is_nan)
	{
		std::cout << "char: Impossible" << std::endl;
		std::cout << "int: Impossible" << std::endl;
	}
	else {
		if (is_printable(static_cast<char>(d)))
			std::cout << "char: " << static_cast<char>(d) << std::endl;
		else
			std::cout << "char: " << "Non displayable" << std::endl;
		std::cout << "int: " << static_cast<int>(d) << std::endl;
	}
	std::cout << "float: " <<  d << ((d - ((int) d) == 0) ? ".0f" : "f") << std::endl;
	std::cout << "double: " << d << ((d - ((int) d) == 0) ? ".0" : "") << std::endl;
}

bool strchr(std::string &s, char c){
	int i = 0;
	while (s[i])
	{
		if (s[i] == c)
			return true;
		i++;
	}
	return false;
}

int convert2int(char *s)
{
	long long i;
	char *end;

	i = std::strtol(s, &end, 10);
	if (*end == '\0' && i > INT_MAX && i < INT_MIN)
	{
		printInt(i);
		return true;
	}
	return false;
}

int convert2float(char *s)
{
	double d;
	char *end;
	std::string v(s);

	d = std::strtod(s, &end);
	if (*end == 'f' && *(end+1) == 0)
	{
		printDouble((float)d, strchr(s, 'n'));
		return true;
	}
	return false;
}

int convert2double(char *s)
{
	double d;
	char *end;

	d = std::strtod(s, &end);
	if (*end == '\0')
	{
		printDouble(d, strchr(s, 'n'));
		return true;
	}
	return false;
}

void print(char *s)
{
	if (convert2float(s)){}
	else if (convert2double(s)) {}
	else if (convert2int(s)) {}
	else if (s[1] == 0)
		printInt(s[0]);
	else
		std::cout << "Never Heard of you\n";
}

int main (int ac, char *av[])
{
	if (ac != 2 || av[1][0] == '\0')
		return 1;
	print(av[1]);
	return 0;
}
